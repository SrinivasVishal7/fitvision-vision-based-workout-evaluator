import cv2.cv2 as cv2
import numpy as np


net = cv2.dnn.readNetFromCaffe('deploy.prototxt.txt', 'res10_300x300_ssd_iter_140000.caffemodel')
cap = cv2.VideoCapture(0)
user_state = [False, False]
counter = 0
decision_limit = 240

while True:
    ret, frame = cap.read()
    (h, w) = frame.shape[:2]
    
    blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
    
    net.setInput(blob)
    detections = net.forward()

    for i in range(detections.shape[2]): 
        confidence = detections[0, 0, i, 2]

        if confidence < 0.5:
            continue

        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (startX, startY, endX, endY) = box.astype("int")

        if startY < decision_limit:
            
            if all(user_state) == True:
                counter+=1
            
            user_state = [False, False] 
            user_state[0] = True
            cv2.line(frame,(0,decision_limit),(frame.shape[1],decision_limit),(0,0,255),1)

        elif startY > decision_limit:
            user_state[1] = True
            cv2.line(frame,(0,decision_limit),(frame.shape[1],decision_limit),(0,255,0),1)

        # cv2.rectangle(frame, (startX, startY), (endX, endY),(0, 255, 0), 2)

    cv2.putText(frame,str(counter),(447,63), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2,(0,255,0),2,cv2.LINE_AA)
    cv2.imshow('frame', frame)
    key = cv2.waitKey(1) & 0xFF

    if key == ord("r"):
        counter = 0
    
    if key == ord("q"):
        break

    


    
	
